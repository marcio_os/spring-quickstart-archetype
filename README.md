Spring 3.2 Quickstart Maven Archetype
=========================================

Summary
-------

Este archetype é uma adaptação de https://github.com/kolorobot/spring-mvc-quickstart-archetype sem spring-mvc, spring-security e classes de exemplo.

The project is a Maven archetype for Spring 3.2 application.

Generated project characteristics
-------------------------
* No-xml Spring 3.2 application
* JUnit/Mockito

Installation
------------

run `mvn install` to install the archetype in your local repository

Create a project
----------------

    mvn archetype:generate
        -DarchetypeGroupId=co.yellowbricks.maven.archetype
        -DarchetypeArtifactId=spring-quickstart
        -DarchetypeVersion=1.0.0-SNAPSHOT
        -DgroupId=my.groupid
        -DartifactId=my-artifactId
        -Dversion=version

Creating a new project in Eclipse
----------------------------------

* Install the archetype in local repository with `mvn install`
* Go to `Preferences > Maven > Archetypes` and `Add Local Catalog`
* Select the catalog from file (`archetype-catalog.xml`)
* Create new Maven project and select the archetype (remember so select `Include snapshot archetypes`).

